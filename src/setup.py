from setuptools import setup

setup(
    name = "fcpu",
    version = "0.0.1",
    author = "Stefan Eletzhofer",
    author_email = "se@nexiles.de",
    description = "a simple 8 bit machine simulator",
    license = "MIT",
    packages=['fcpu'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)