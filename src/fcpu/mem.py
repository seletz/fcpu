class Core:
    """
    Core Memory

    Initialization:

    >>> core = Core(1024)
    >>> len(core)
    1024

    Set some value:

    >>> core.poke(0x100, 0xff)
    >>> core.peek(0x100)
    255

    The memory is circular -- writing non-existent addresses "wrap":

    >>> core.poke(0x100f, 0xee)
    >>> core.peek(0x100f) == core.peek(0x000f)
    True

    Loading binary data is easy:

    >>> core.load_binary([0xde, 0xad, 0xbe, 0xef])
    >>> core.peek(0x0003) == 0xef
    True

    Ans so is loading hex dumps:

    >>> core.load_hex("de ad be ef")
    >>> core.peek(0x0003) == 0xef
    True


    """
    def __init__(self, size):
        self.size = size
        self.core = bytearray().zfill(size).replace("0", chr(0))

    def __len__(self):
        return len(self.core)

    def peek(self, adr):
        adr = adr % self.size
        return self.core[adr % self.size] & 0xff

    def poke(self, adr, val):
        self.core[adr % self.size] = val & 0xff

    def pokew(self, adr, value):
        self.poke(adr, value)
        self.poke(adr + 1, value>>8)

    def peekw(self, adr):
        return 0xffff&(self.peek(adr) + (self.peek(adr + 1)<<8))

    def load_binary(self, adr, data):
        data = bytearray(data)
        k = len(data)
        self.core = self.core[:adr] + data + self.core[adr +k:]

    def load_hex(self, adr, data):
        data = bytearray().fromhex(data)
        k = len(data)
        self.core = self.core[:adr] + data + self.core[adr +k:]

    def hexdump(self, start=0, num=0xff):
        num = min(self.size, num)
        num = (num % 16) + 1

        print "%d lines starting from %04x" % (num, start)

        adr = start
        for line in range(0, num):
            print "0x%04x: " % adr,
            for b in range(0, 16):
                print "%02x" % self.peek(adr + b),
                if b == 7:
                    print "  ",
            print
            adr = adr + 16




