import struct
import logging

logging.basicConfig(level=logging.DEBUG)

from fcpu import opcodes

__doc__ = """

FCPU Spec -- loosely based on http://www.6502.org/tutorials/6502opcodes.html

Registers: A, X, Y, PC

Basic cycle:
- Fetch
- Decode
- Execute


Instruction Set:

LDA
LDX
LDY

STA
STX
STY

ADC

JSR
RTS

BNE

PHA
PHX
PHY

Adressing Modes:

Immediate:   LDA #15
Absolute:    LDA $0100
Zero Page:   LDX $40
Indirect:    JMP ($1000)
Absolute Indexed: STA $1000, Y

Endianess:  LITTLE ENDIAN  (LO HI)

1080: ff
...
2000: 80 10

LDA ($2000)  ; A <= ff
=>

"""

class Register:
    def __init__(self, name):
        self.name = name
        self.value = 0

    def set(self, value):
        self.value = value

    def inc(self):
        self.value += 1

    def set_bit(self, nr):
        self.value |= 1<<nr

    def clear_bit(self, nr):
        self.value &= ~(1<<nr)

    def test_bit(self, nr):
        return self.value | (1<<nr)

    def __and__(self, other):
        return self.value & other

    def __or__(self, other):
        return self.value | other

    def __xor__(self, other):
        return self.value ^ other

    def __neg__(self):
        return ~self.value

    def __repr__(self):
        return "{}: {} {}".format(self.name, hex(self.value), bin(self.value))


class FCPU:
    VECTOR_RESET = 0xffff - 2
    VECTOR_NMI   = 0xffff - 4
    VECTOR_IRQ   = 0xffff - 6

    FLAGS_BIT_N  = 7
    FLAGS_BIT_C  = 1
    FLAGS_BIT_Z  = 0

    def __init__(self, core):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug("init")

        self.PC = Register("PC")
        self.A = Register("A")
        self.X = Register("X")
        self.Y = Register("Y")
        self.P = Register("P")

        self.core = core

        self.OP = Register("OP")

        self.opcodes = opcodes.get_opcodes()

    def error(self, msg, *args, **kw):
        self.logger.error("ERROR @ 0x%04x:" % self.PC.value, msg, *args, **kw)

    def reset(self):
        self.logger.debug("RESET")
        # load PC with contents of reset vector
        self.PC.value = self.core.peekw(self.VECTOR_RESET)

        # clear Flags
        self.P.value = 0

    def halt(self):
        self.logger.error("HALT")

    def step(self):
        pc = self.PC.value
        self.fetch()
        self.decode()
        if self.operand:
            print "{pc:04x}: {mnemonic} {operand:04x} [{mode}]".format(pc=pc, operand=self.operand, **self.opcode)
        else:
            print "{pc:04x}: {mnemonic} [{mode}]".format(pc=pc, **self.opcode)
        self.execute()

    def fetch(self):
        self.logger.debug("FETCH")
        self.OP.value = self.core.peek(self.PC.value)
        self.PC.inc()

    def decode(self):
        self.logger.debug("DECODE")
        self.opcode = None

        # determine opcode valid
        opcode = self.opcodes.get(self.OP.value)
        if not opcode:
            self.error("Invalid opcode %02x" % self.OP.value)
            opcode= self.opcodes[0xea]  # NOP

        self.logger.debug("DECODE: opcode: %r", opcode)
        self.opcode = opcode

        # fetch operands if needed
        self.operand = self.fetch_operand(opcode)
        if self.operand:
            self.logger.debug("DECODE: operand: %04x", self.operand)

    def execute(self):
        self.logger.debug("EXECUTE")
        # fetch microcode handler
        handler = getattr(self, "microcode_%s"%self.opcode["mnemonic"])
        if not handler:
            self.logger.error("Unhandled opcode: {mnemonic}".format(**self.opcode))
            self.halt()
            return

        # execute
        handler()

    def fetch_operand(self, opcode):
        operand = None
        if opcode["ops"] not in [0, 1, 2]:
            self.error("Invalid operand count:", opcode)
            self.halt()
            return

        if opcode["ops"] == 1:
            operand = self.core.peek(self.PC.value)
            self.PC.inc()

        elif opcode["ops"] == 2:
            operand = self.core.peekw(self.PC.value)
            self.PC.inc()
            self.PC.inc()

        return operand

    #####################################################################################
    # addressing modes
    #####################################################################################

    def immediate_p(self, op):
        return op["mode"].lower() == "immediate"

    def absolute_p(self, op):
        return op["mode"].lower() == "absolute"

    def absolute_x_p(self, op):
        return op["mode"].lower() == "absolute,x"

    def absolute_y_p(self, op):
        return op["mode"].lower() == "absolute,y"

    def zeropage_p(self, op):
        return op["mode"].lower() == "zero page"

    def zeropage_x_p(self, op):
        return op["mode"].lower() == "zero page,x"

    def zeropage_y_p(self, op):
        return op["mode"].lower() == "zero page,y"

    def relative_p(self, op):
        return op["mode"].lower() == "relative"

    def indirect_p(self, op):
        return op["mode"].lower() == "indirect"

    def indexed_indirect_p(self, op):
        return op["mode"].lower() == "indexed indirect"

    def indirect_indexed_p(self, op):
        return op["mode"].lower() == "indirect indexed"

    def operand_address(self, op):
        if self.absolute_p(op):
            adr = self.operand & 0xffff

        elif self.indirect_p(op):
            # JMP $20        PC = MEMw[$20]
            adr = self.operand & 0xff

        elif self.absolute_x_p(op):
            adr = (self.operand + self.X.value) & 0xffff

        elif self.absolute_y_p(op):
            adr = (self.operand + self.Y.value) & 0xffff

        elif self.zeropage_p(op):
            adr = self.operand & 0xff

        elif self.zeropage_x_p(op):
            # LDA $20, X      A = MEMb[$20 + X]
            adr = (self.operand + self.X.value) & 0xff

        elif self.zeropage_y_p(op):
            # LDA $20, Y      A = MEMb[$20 + Y]
            adr = (self.operand + self.Y.value) & 0xff

        elif self.indexed_indirect_p(op):
            # LDA ($20, X)    A = MEM[$20 + X]
            adr = (self.operand + self.X.value) & 0xff

        elif self.indirect_indexed_p(op):
            # LDA ($20), Y    A = MEM[MEMw[$20] + Y]
            adr = self.core.peekw(self.operand & 0xff)  # zero page
            adr += self.Y.value & 0xff
        else:
            self.logger.error("adressing mode not yet implemented: ", op)
            self.halt()
            return 0xffff

        return adr

    def load_byte(self, op):
        if self.immediate_p(op):
            return self.operand & 0xff

        adr = self.operand_address(op)
        return self.core.peek(adr)

    def load_word(self, op):
        if self.immediate_p(op):
            return self.operand & 0xffff

        adr = self.operand_address(op)
        return self.core.peekw(adr)

    def store_byte(self, op, value):
        adr = self.operand_address(op)
        self.core.poke(adr, value)


    #####################################################################################
    # microcode
    #####################################################################################
    def microcode_NOP(self):
        pass

    def microcode_LDA(self):
        self.A.value = self.load_byte(self.opcode)

        if self.A.value == 0:
            self.P.set_bit(self.FLAGS_BIT_Z)
        if self.A.value & 0x80:
            self.P.set_bit(self.FLAGS_BIT_N)

    def microcode_STA(self):
        self.store_byte(self.opcode, self.A.value)

    def microcode_INC(self):
        value = self.load_byte(self.opcode)
        value += 1
        self.store_byte(self.opcode, value)

        if value == 0:
            self.P.set_bit(self.FLAGS_BIT_Z)
        if value & 0x80:
            self.P.set_bit(self.FLAGS_BIT_N)

    def microcode_BRA(self):
        value = struct.unpack("b", chr(self.operand & 0xff))[0]
        if value < 0:
            self.PC.value = self.PC.value + value - 2
        else:
            self.PC.value = self.PC.value + value

    def microcode_JMP(self):
        adr = self.load_word(self.opcode)
        self.PC.value = adr



