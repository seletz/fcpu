from fcpu.mem import Core

class TestCore:

    def test_len(self):
        assert len(Core(1024)) == 1024

    def test_ppe(self):
        core = Core(1024)

        core.poke(0x100, 0xff)
        assert core.peek(0x100) == 0xff

    def test_ppe16(self):
        core = Core(1024)

        core.pokew(0x100, 0xdead)
        core.hexdump(0x100, 1)
        assert core.peekw(0x100) == 0xdead

        assert core.peek(0x100) == 0xad
        assert core.peek(0x101) == 0xde

    def test_memwrap(self):
        core = Core(1024)
        core.poke(0x100f, 0xff)
        assert core.peek(0x000f) == 0xff

    def test_load_hex(self):
        core = Core(1024)
        core.load_hex(0, "de ad be ef")
        assert core.peek(0x0003) == 0xef

    def test_load_bin(self):
        core = Core(1024)
        core.load_binary(0, [0xde, 0xad, 0xbe, 0xef])
        assert core.peek(0x0003) == 0xef
