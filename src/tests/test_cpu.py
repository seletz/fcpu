from fcpu.mem import Core
from fcpu.cpu import Register
from fcpu.cpu import FCPU

class TestRegister:

    def test_repr(self):
        PC = Register("PC")
        assert repr(PC) == "PC: 0x0 0b0"

        PC.value = 0xf0
        assert repr(PC) == "PC: 0xf0 0b11110000"

class TestCPU:

    def make_cpu(self):
        core = Core(2**16)
        core.pokew(FCPU.VECTOR_RESET, 0xc000)
        cpu = FCPU(core)
        return cpu

    def test_reset(self):
        cpu = self.make_cpu()
        cpu.reset()

        assert cpu.P.value == 0x00
        assert cpu.PC.value == 0xc000

    def test_loop(self):
        cpu = self.make_cpu()
        cpu.reset()

        # cc00:  NOP           ea
        # cc01:  LDA $20       a5 20
        # cc03:  STA $40       85 40
        # cc05:  BRA -4        80 fc

        cpu.core.load_hex(0xc000, "ea a5 20 85 40 80 fc")
        cpu.core.hexdump(0xc000)

        cpu.core.poke(0x20, 0xaa)
        cpu.core.poke(0x40, 0xff)

        assert cpu.PC.value == 0xc000

        # NOP
        cpu.fetch()
        assert cpu.PC.value == 0xc001
        assert cpu.OP.value == 0xea

        cpu.decode()
        assert cpu.opcode
        assert cpu.opcode["mnemonic"].startswith("NOP")
        assert cpu.PC.value == 0xc001

        cpu.execute()
        assert cpu.PC.value == 0xc001

        # LDA $20
        cpu.fetch()
        assert cpu.PC.value == 0xc002
        assert cpu.OP.value == 0xa5

        cpu.decode()
        assert cpu.opcode["mnemonic"].startswith("LDA")
        assert cpu.PC.value == 0xc003

        cpu.execute()
        assert cpu.PC.value == 0xc003
        assert cpu.A.value == 0xaa

        # STA $40
        cpu.fetch()
        assert cpu.PC.value == 0xc004
        assert cpu.OP.value == 0x85

        cpu.decode()
        assert cpu.opcode["mnemonic"].startswith("STA")
        assert cpu.PC.value == 0xc005

        cpu.execute()
        assert cpu.PC.value == 0xc005
        assert cpu.A.value == 0xaa
        assert cpu.core.peek(0x40) == 0xaa

        # BRA -4
        cpu.fetch()
        assert cpu.PC.value == 0xc006
        assert cpu.OP.value == 0x80

        cpu.decode()
        assert cpu.opcode["mnemonic"].startswith("BRA")
        assert cpu.PC.value == 0xc007

        cpu.execute()
        assert cpu.PC.value == 0xc001

    def test_loop_2(self):
        cpu = self.make_cpu()

        # cc00:  NOP           ea
        # cc01:  LDA $20       a5 20
        # cc03:  STA $40       85 40
        # cc05:  BRA -4        80 fc

        cpu.core.load_hex(0xc000, "ea a5 20 85 40 80 fc")
        cpu.core.poke(0x20, 0xaa)
        cpu.core.poke(0x40, 0xff)

        cpu.reset()
        assert cpu.PC.value == 0xc000

        for cycle in range(5):
            cpu.step()

        assert cpu.PC.value == 0xc003


